"#README" 

There are 4 sub folders in this repository

- Extracted: The place that all sub squares extracted from sudoku matrix are stored. 
- Test: used for test svm model of the app.
- Train: include upto 5350 images for training svm model.
- TestCasesForFinalApp: Test cases for testing app. 

How to use?
- Step 1: clone the source code.

- Step 2: Open directory where the source code is stored.

- Step 3: Run Command Prompt from this folder.

- Step 4: type "python SDKDetector.py -q <Directory to the test image>" without quote sign. 

- Step 5: Press Enter

